# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: Assima, Arthur, koboyodaarthur.assima.etu@univ-lille.fr

- Nom, Prénom, email: El Ammari, Nordine, nordine.elammari.etu@univ-lille.fr

## Question 1

Le processus ne peut pas écrire car toto étant le propriétaire, les permissions du propriétaire sont utilisées (r--) même s'il appartient au groupe ubuntu.

## Question 2

Le caractère x sur un répertoire signifie que l'on peut entrer et effectuer les actions autorisées sur les fichiers du répertoire. S'il n'est pas présent on ne peut que lister les fichiers qui le composent.
Lorsqu'on tente d'entrer dans mydir avec cd mydir/ la permission est refusée car le groupe n'a pas la permission d'exécution.
Lorsqu'on tente d'afficher le contenu de mydir avec ls -al mydir/ la permission est refusée pour la même raison.

## Question 3

Tous les ID sont 1001, l'utilisateur toto n'a pas l'autorisation de lire le fichier, le processus ne peut donc pas être exécuté

```
toto@vm1:/home/ubuntu$ ./suid
EUID: 1001
EGID: 1001
RUID: 1001
RGID: 1001
Cannot open file
```


EUID devient 1001, les autres ID restent inchangés.L'utilisateur toto peut lire le fichier à ce moment, et le processus peut s'exécuter.

```
ubuntu@vm1:~$ chmod u+s suid
toto@vm1:/home/ubuntu$ ./suid
EUID is 1000
EGID is 1001
RUID is 1001
RGID is 1001
hello world
```

## Question 4

L'EUID n'a pas changé.

```
ubuntu@vm1:~$ chmod  u+s suid.py
toto@vm1:/home/ubuntu$ python3 suid.py
EUID:  1001
EGID:  1001
```

## Question 5

chfn permet de change le nom et les informations d'un utilisateur.

Root a tout les droits sur le fichiers et le reste des utilisateurs n'ont que les droits d'ecriture et de lecture 

```
toto@vm1:/home/ubuntu$ ls -al /usr/bin/chfn
-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn
```

Le caractère s signifie que d'autre utilisateur que root peuvent executer le fichier si suid est utilisé.

## Question 6

Les mots de passe sont stockés dans le fichier '/etc/shadow',ils sont hachés avec une méthode représenter par un chiffre en deux
signe de dollars.

les mots de passe ne sont pas stockés dans '/etc/passwd' car tout les utilisateurs y ont accès, cependant seul l'administrateur
a acccès à '/etc/shadow', si un utilisateur essais d'y accéder l'incident sera reporté à l'administrateur.

## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests. 








